# SIAMES Autotagger

Use Spacy PhraseMatcher to automatically tag corpus to train NER model later. Set constants in `siamesnlp/tagger.py` and then run

```
pip install -r requirements
python siamesnlp/tagger.py
```
