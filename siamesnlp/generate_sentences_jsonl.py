#!/usr/bin/env python3

from os import path

import spacy
from tagger import retrieve_doc
from tagger import OUTPUT


def get_line(text):
    clean_sentence = ' '.join([token.text for token in text if token.is_alpha])
    if clean_sentence:
        return '{{\"text\": "{}"}}\n'.format(clean_sentence)
    return ''


def run():
    nlp = spacy.load("en_core_web_sm", disable=['tagger', 'parser', 'ner'])
    sentencizer = nlp.create_pipe("sentencizer")
    nlp.add_pipe(sentencizer)
    with open(path.join(OUTPUT, 'sentences.jsonl'), 'w') as file:
        for doc, _ in retrieve_doc(nlp):
            [file.write(get_line(sent)) for sent in doc.sents]


if __name__ == '__main__':
    run()
