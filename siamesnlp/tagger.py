import csv
import fileinput
import logging
import os
import re
import time
from os import path

import numpy as np
import spacy
from spacy.attrs import LEMMA
from owlready2 import get_ontology
from pandas import DataFrame
from spacy.matcher import PhraseMatcher

INPUT_DOCS_PATH = 'docs_bw'
RESULTS_PATH = 'results'
SCORES_FILE_NAME = 'scores'
FILE_NAME = 'ontologies/siames.xml'
OUTPUT = 'out'
BREAKLINE = '\n'
SPACY_MODEL = 'en_core_web_lg'

TO_CSV_OUTPUT_OPTIONS = {'sep': ' ', 'index': False, 'header': False, 'doublequote': False,
                         'quoting': csv.QUOTE_NONE, 'escapechar': ' ', 'columns': ['text', 'ner']}

FEATURES = ['indicators', 'outcomes', 'stakeholders', 'sectors', 'proxies', 'sources']

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level='INFO')


def save_entities_lists():
    logging.info('Loading ontology')
    get_ontology("file://ontologies/sustainability.owl").load()
    onto = get_ontology('file://{}'.format(FILE_NAME)).load()

    indicators = get_entities_list(onto, onto.Indicator)
    stakeholders = get_entities_list(onto, onto.Stakeholder)
    sectors = get_entities_list(onto, onto.Sector)
    outcomes = get_entities_list(onto, onto.Outcome)
    proxies = get_entities_list(onto, onto.Proxy)
    sources = get_entities_list(onto, onto.Source)

    entities_to_file(indicators, 'indicators')
    entities_to_file(stakeholders, 'stakeholders')
    entities_to_file(outcomes, 'outcomes')
    entities_to_file(sectors, 'sectors')
    entities_to_file(proxies, 'proxies')
    entities_to_file(sources, 'sources')

    return {'indicators': len(indicators),
            'stakeholders': len(stakeholders),
            'outcomes': len(outcomes),
            'sectors': len(sectors),
            'proxies': len(proxies),
            'sources': len(sources),}


def get_entities_list(onto, type):
    entities = []
    for entity in onto.search(type=type):
        if entity.has_literal[0].replace('_', '') not in entities:
            entities.append(entity.has_literal[0].replace('_', ''))
    return entities


def entities_to_file(entities, name):
    with open(path.join(OUTPUT, name), 'w') as file:
        for entity in entities:
            file.write('{}{}'.format(entity, '\n'))


def get_entities(doc, nlp):
    found = {}
    full_entities = DataFrame(columns=['ner'])
    take_tagged = lambda s1, s2: np.where(s1 != 'O', s1, s2)
    for feature in FEATURES:
        logging.info('Tagging {}'.format(feature))
        entities = DataFrame(columns=['text', 'ner'])
        matcher = PhraseMatcher(nlp.vocab, attr=LEMMA)
        with open(path.join(OUTPUT, feature)) as features:
            terms = [term for term in features.read().split('\n') if term]
        patterns = [[text, nlp(text)] for text in terms]
        for text, pattern in patterns:
            matcher.add(text, None, pattern)

        matches = matcher(doc)

        match_id, start, end = [(-1,), (-1,), (-1,)] if not matches else zip(*[match for match in matches])

        found[feature] = set(match_id)

        position = -1
        next_tag = 'O'
        for token in map(str.strip, [token.text for token in doc]):
            position += 1
            if position in start:
                entities = entities.append({'text': token, 'ner': '{}{}'.format('B-', feature)}, ignore_index=True)
                next_tag = '{}{}'.format('I-', feature)
                continue
            if position in end:
                next_tag = 'O'
            if BREAKLINE in token or not token:
                entities = entities.append({'text': token, 'ner': ''}, ignore_index=True)
                continue
            entities = entities.append({'text': token, 'ner': next_tag}, ignore_index=True)
        if full_entities.empty:
            full_entities = entities
            continue
        full_entities = full_entities.combine(entities, take_tagged)
    logging.info('Entities tagged')
    return full_entities, found


def get_pos(doc):
    pos = DataFrame(columns=['text', 'pos'])
    for token in doc:
        if BREAKLINE in token.text or not token.text:
            pos = pos.append({'text': '\n', 'pos': ''}, ignore_index=True)
            continue
        pos = pos.append({'text': token.text, 'pos': token.tag_}, ignore_index=True)
    return pos


def get_noun_chunks(doc):
    noun_chunks = DataFrame(columns=['noun'])
    for token in doc:
        if BREAKLINE in token.text or not token.text:
            noun_chunks = noun_chunks.append({'noun': ''}, ignore_index=True)
            continue
        noun_chunks = noun_chunks.append({'noun': 'O'}, ignore_index=True)
    return noun_chunks


def retrieve_doc(nlp):
    for filepath in os.scandir(INPUT_DOCS_PATH):
        with open(filepath, 'r') as file_to_read:
            yield nlp(preprocess_doc(file_to_read.read())), filepath


def preprocess_doc(text):
    text = re.sub(r'([a-z])([A-Z])', r'\g<1> \g<2>', text)
    text = re.sub(r'\s{2,}', '\n', text)
    return text.lower()


def run():
    current_file = 0
    total_files = len(os.listdir(INPUT_DOCS_PATH))
    logging.info('Running')
    full_data = DataFrame()

    logging.info('Saving entities')
    features_total = save_entities_lists()
    logging.info(features_total)

    spacy.prefer_gpu()
    nlp = spacy.load(SPACY_MODEL)
    full_found = {}
    full_not_found = []
    for doc, filepath in retrieve_doc(nlp):
        logging.info('Tagging {} {:.2f}% completed'.format(filepath.name, current_file * 100 / total_files))
        current_file += 1
        entities, found = get_entities(doc, nlp)
        logging.info('Add results to global data')
        full_data = full_data.append(entities)
        full_found.update(
            {k: b.union(full_found.get(k)) if full_found.get(k) is not None else b for k, b in found.items()})
        [logging.info('Found {} {}'.format(len(found_items), feature)) for feature, found_items in full_found.items()]

    for feature, found_items in full_found.items():
        with open(path.join(OUTPUT, feature)) as features:
            terms = [term for term in features.read().split('\n') if term]
            not_found = [term for term in terms if
                         term not in [nlp.vocab[term].text for term in found_items if term >= 0]]
            full_not_found.extend(not_found)
            logging.warning('{} {} found'.format(len(found_items), feature))
            logging.warning('{} {} not found'.format(len(not_found), feature))
            [logging.info(term) for term in not_found]

    [logging.info('Found {} {}'.format(len(found_items), feature)) for feature, found_items in full_found.items()]

    logging.info('Writing results files')
    with open(path.join(RESULTS_PATH, '{}{}'.format(SCORES_FILE_NAME, time.strftime("-at-%Y%m%d-%H%M%S"))), 'w') as file:
        [file.write('Initial {} {}\n'.format(len(found_items), feature)) for feature, found_items in full_found.items()]
        file.write('-------------\n')
        [file.write('Found {} {}\n'.format(feature_name, feature_total)) for feature_name, feature_total in features_total.items()]
        file.write('\n\n-------------\nNOT FOUND\n')
        [file.write('{}\n'.format(term)) for term in full_not_found]
    piece_size = int(len(full_data) / 5)
    test = full_data.iloc[:piece_size]
    valid = full_data.iloc[piece_size:piece_size * 2]
    train = full_data.iloc[piece_size * 2:]
    full_data.to_csv(path.join(RESULTS_PATH, 'results.csv'), **TO_CSV_OUTPUT_OPTIONS)
    test.to_csv(path.join(RESULTS_PATH, 'test.txt'), **TO_CSV_OUTPUT_OPTIONS)
    valid.to_csv(path.join(RESULTS_PATH, 'valid.txt'), **TO_CSV_OUTPUT_OPTIONS)
    train.to_csv(path.join(RESULTS_PATH, 'train.txt'), **TO_CSV_OUTPUT_OPTIONS)

    trim_files()

    logging.info('Finished!')


def trim_files():
    files = (
        path.join(RESULTS_PATH, 'results.csv'),
        path.join(RESULTS_PATH, 'test.txt'),
        path.join(RESULTS_PATH, 'valid.txt'),
        path.join(RESULTS_PATH, 'train.txt'))
    with fileinput.input(files=files, inplace=True) as f:
        for line in f:
            print(line.strip())


if __name__ == '__main__':
    run()
