import csv
import fileinput
import logging
import os
import re
from os import path

import numpy as np
import spacy
from spacy.attrs import LEMMA
from owlready2 import get_ontology
from pandas import DataFrame
from spacy.matcher import PhraseMatcher

INPUT_DOCS_PATH = 'docs_bw'
RESULTS_PATH = 'results'
FILE_NAME = 'ontologies/siames.xml'
OUTPUT = 'out'
BREAKLINE = '\n'

TO_CSV_OUTPUT_OPTIONS = {'sep': ' ', 'index': False, 'header': False, 'doublequote': False,
                         'quoting': csv.QUOTE_NONE, 'escapechar': ' ', 'columns': ['text', 'ner']}

FEATURES = ['indicators', 'outcomes', 'stakeholders', 'sectors']
# FEATURES = ['sectors']

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level='INFO')


def save_entities_lists():
    logging.info('Loading ontology')
    sustainability_onto = get_ontology("file://ontologies/sustainability.owl").load()
    onto = get_ontology('file://{}'.format(FILE_NAME)).load()
    indicators = []
    stakeholders = []
    sectors = []
    outcomes = []

    for case in onto.search(type=onto.Case):
        for indicator, onto_indicator in [[indicator.has_literal[0].replace('_', ''), indicator] for indicator in
                                          case.has_indicator]:
            if indicator not in indicators:
                indicators.append(indicator)
            for stakeholder in [stakeholder.has_literal[0].replace('_', '') for stakeholder in
                                onto_indicator.has_receiver]:
                if stakeholder not in stakeholders:
                    stakeholders.append(stakeholder)
        for outcome in [outcome.has_literal[0].replace('_', '') for outcome in case.has_outcome]:
            if outcome not in outcomes:
                outcomes.append(outcome)
        for sector in [sector.has_literal[0].replace('_', '') for sector in case.has_sector]:
            if sector not in sectors:
                sectors.append(sector)
    with open(path.join(OUTPUT, 'indicators'), 'w') as indicators_file:
        for indicator in indicators:
            indicators_file.write('{}{}'.format(indicator, '\n'))
    with open(path.join(OUTPUT, 'stakeholders'), 'w') as stakeholders_file:
        for stakeholder in stakeholders:
            stakeholders_file.write('{}{}'.format(stakeholder, '\n'))
    with open(path.join(OUTPUT, 'outcomes'), 'w') as outcomes_file:
        for outcome in outcomes:
            outcomes_file.write('{}{}'.format(outcome, '\n'))
    with open(path.join(OUTPUT, 'sectors'), 'w') as sectors_file:
        for sector in sectors:
            print(sector, file=sectors_file)

    logging.info('{} indicators, {} stakeholders, {} outcomes and {} sectors to tag corpus'.format(len(indicators),
                                                                                                   len(stakeholders),
                                                                                                   len(outcomes),
                                                                                                   len(sectors)))


def get_entities(doc, nlp):
    found = {}
    full_entities = DataFrame(columns=['ner'])
    take_tagged = lambda s1, s2: np.where(s1 != 'O', s1, s2)
    for feature in FEATURES:
        logging.info('Tagging {}'.format(feature))
        entities = DataFrame(columns=['text', 'ner'])
        matcher = PhraseMatcher(nlp.vocab, attr=LEMMA)
        with open(path.join(OUTPUT, feature)) as features:
            terms = [term for term in features.read().split('\n') if term]
        # Only run nlp.make_doc to speed things up
        patterns = [[text, nlp(text)] for text in terms]
        for text, pattern in patterns:
            matcher.add(text, None, pattern)

        matches = matcher(doc)

        match_id, start, end = [(-1,), (-1,), (-1,)] if not matches else zip(*[match for match in matches])

        found[feature] = set(match_id)

        position = -1
        next_tag = 'O'
        for token in map(str.strip, [token.text for token in doc]):
            position += 1
            if position in start:
                entities = entities.append({'text': token, 'ner': '{}{}'.format('B-', feature)}, ignore_index=True)
                next_tag = '{}{}'.format('I-', feature)
                continue
            if position in end:
                next_tag = 'O'
            if BREAKLINE in token or not token:
                entities = entities.append({'text': token, 'ner': ''}, ignore_index=True)
                continue
            entities = entities.append({'text': token, 'ner': next_tag}, ignore_index=True)
        if full_entities.empty:
            full_entities = entities
            continue
        full_entities = full_entities.combine(entities, take_tagged)
    logging.info('Entities tagged')
    return full_entities, found


def get_pos(doc):
    pos = DataFrame(columns=['text', 'pos'])
    for token in doc:
        if BREAKLINE in token.text or not token.text:
            pos = pos.append({'text': '\n', 'pos': ''}, ignore_index=True)
            continue
        pos = pos.append({'text': token.text, 'pos': token.tag_}, ignore_index=True)
    return pos


def get_noun_chunks(doc):
    noun_chunks = DataFrame(columns=['noun'])
    for token in doc:
        if BREAKLINE in token.text or not token.text:
            noun_chunks = noun_chunks.append({'noun': ''}, ignore_index=True)
            continue
        noun_chunks = noun_chunks.append({'noun': 'O'}, ignore_index=True)
    return noun_chunks


def retrieve_doc(nlp):
    for filepath in os.scandir(INPUT_DOCS_PATH):
        with open(filepath, 'r') as file_to_read:
            yield nlp(file_to_read.read()), filepath


def run():
    current_file = 0
    total_files = len(os.listdir(INPUT_DOCS_PATH))
    logging.info('Running')
    full_data = DataFrame()

    logging.info('Saving entities')
    save_entities_lists()

    spacy.prefer_gpu()
    nlp = spacy.load("en_core_web_sm")
    full_found = {}
    for doc, filepath in retrieve_doc(nlp):
        logging.info('Tagging {} {:.2f}% completed'.format(filepath.name, current_file * 100 / total_files))
        current_file += 1
        entities, found = get_entities(doc, nlp)
        logging.info('Add results to global data')
        full_data = full_data.append(entities)
        full_found.update(
            {k: b.union(full_found.get(k)) if full_found.get(k) is not None else b for k, b in found.items()})
        [logging.info('Found {} {}'.format(len(found_items), feature)) for feature, found_items in full_found.items()]

    for feature, found_items in full_found.items():
        with open(path.join(OUTPUT, feature)) as features:
            terms = [term for term in features.read().split('\n') if term]
            not_found = [term for term in terms if
                         term not in [nlp.vocab[term].text for term in found_items if term >= 0]]
            logging.warning('{} {} found'.format(len(found_items), feature))
            logging.warning('{} {} not found'.format(len(not_found), feature))
            [logging.info(term) for term in not_found]

    [logging.info('Found {} {}'.format(len(found_items), feature)) for feature, found_items in full_found.items()]

    logging.info('Writing results files')
    piece_size = int(len(full_data) / 5)
    test = full_data.iloc[:piece_size]
    valid = full_data.iloc[piece_size:piece_size * 2]
    train = full_data.iloc[piece_size * 2:]
    full_data.to_csv(path.join(RESULTS_PATH, 'results.csv'), **TO_CSV_OUTPUT_OPTIONS)
    test.to_csv(path.join(RESULTS_PATH, 'test.txt'), **TO_CSV_OUTPUT_OPTIONS)
    valid.to_csv(path.join(RESULTS_PATH, 'valid.txt'), **TO_CSV_OUTPUT_OPTIONS)
    train.to_csv(path.join(RESULTS_PATH, 'train.txt'), **TO_CSV_OUTPUT_OPTIONS)

    trim_files()

    logging.info('Finished!')


def trim_files():
    files = (
        path.join(RESULTS_PATH, 'results.csv'),
        path.join(RESULTS_PATH, 'test.txt'),
        path.join(RESULTS_PATH, 'valid.txt'),
        path.join(RESULTS_PATH, 'train.txt'))
    with fileinput.input(files=files, inplace=True) as f:
        for line in f:
            print(line.strip())


def get_line(text):
    return '{}{}'.format(' '.join([token.text for token in text if token.is_alpha]), '\n')


def test():
    nlp = spacy.load('en_core_web_sm', disable=['parser', 'ner'])
    with open('docs_bw/007.txt', 'r') as file:
        text = re.sub(r'([a-z])([A-Z])', r'\g<1> \g<2>', file.read())
        text = re.sub(r'\s{2,}', '\n', text)
        doc = nlp(text)
    matcher = PhraseMatcher(nlp.vocab, attr=LEMMA)
    matcher.add('INDICATOR', None, nlp('banding together with others of like mind'))

    matches = matcher(doc)
    print(matches)


if __name__ == '__main__':
    test()
