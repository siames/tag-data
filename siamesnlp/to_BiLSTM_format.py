import csv
from os import path

import pandas

RESULTS_PATH = 'results'
test = pandas.read_csv('test.txt', sep=' ', names=['text', 'pos', 'chunk', 'ner'], quoting=csv.QUOTE_NONE)
valid = pandas.read_csv('valid.txt', sep=' ', names=['text', 'pos', 'chunk', 'ner'], quoting=csv.QUOTE_NONE)
train = pandas.read_csv('train.txt', sep=' ', names=['text', 'pos', 'chunk', 'ner'], quoting=csv.QUOTE_NONE)

valid = valid[valid.text != '\t']
test = test[test.text != '\t']
train = train[train.text != '\t']

print(valid.head(20))


test.to_csv(path.join(RESULTS_PATH, 'example.test'), sep=' ', index=False, header=False, doublequote=False,
            quoting=csv.QUOTE_NONE, escapechar=' ', columns=['text', 'ner'])
valid.to_csv(path.join(RESULTS_PATH, 'example.dev'), sep=' ', index=False, header=False, doublequote=False,
             quoting=csv.QUOTE_NONE, escapechar=' ', columns=['text', 'ner'])
train.to_csv(path.join(RESULTS_PATH, 'example.train'), sep=' ', index=False, header=False, doublequote=False,
             quoting=csv.QUOTE_NONE, escapechar=' ', columns=['text', 'ner'])